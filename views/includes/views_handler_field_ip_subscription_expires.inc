<?php 

class views_handler_field_ip_subscription_expires extends views_handler_field {
	
	function option_definition() {
		$options = parent::option_definition();
		$options['custom_date_format'] = array('default' => 'Y-m-d H:i:s');
		return $options;
	}

	function options_form(&$form, &$form_state) {
		parent::options_form($form, $form_state);
	
		$form['date_format'] = array(
			'#type' => 'textfield',
			'#title' => t('Custom date format'),
			'#description' => t('If "Custom", see <a href="http://us.php.net/manual/en/function.date.php" target="_blank">the PHP docs</a> for date formats. If "Time ago" this is the the number of different units to display, which defaults to two.'),
			'#default_value' => isset($this->options['custom_date_format']) ? $this->options['custom_date_format'] : '',
		);
	}

	function render($values) {
		$value = $values->{$this->field_alias};
		$format = $this->options['date_format'];
		return (!$value) ? theme('views_nodate') : date($format, $value);
	}
}
