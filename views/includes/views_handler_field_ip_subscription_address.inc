<?php

class views_handler_field_ip_subscription_address extends views_handler_field {
	
	function render($values) {
		$items = array();
		$query = db_query("SELECT INET_NTOA(inetmin) AS ip_min, INET_NTOA(inetmax) AS ip_max FROM {ip_subscription_address} WHERE sid = %d", $values->{$this->field_alias});
		while ($result = db_fetch_object($query)) {
			$items[] = ($result->ip_min == $result->ip_max) ? $result->ip_min : $result->ip_min . ' - ' . $result->ip_max;
		}
		return implode(', ', $items);
	}
}
