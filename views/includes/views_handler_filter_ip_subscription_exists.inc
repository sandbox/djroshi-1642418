<?php 

class views_handler_filter_ip_subscription_exists extends views_handler_filter {

	function admin_summary() { }
 	function operator_form() { }

	function query() {
		$table = $this->ensure_my_table();
		$this->query->add_where($this->options['group'], "$this->table_alias.sid IS NOT NULL");
	}
}
