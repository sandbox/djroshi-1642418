<?php

/**
 * Implementation of hook_views_handlers().
 */
function ip_subscription_views_handlers() {
	return array(
		'info' => array(
			'path' => drupal_get_path('module', 'ip_subscription') . '/views/includes',
		),
		'handlers' => array(
			'views_handler_field_ip_subscription_address' => array(
				'parent' => 'views_handler_field',
			),
			'views_handler_field_ip_subscription_expires' => array(
				'parent' => 'views_handler_field',
			),
			'views_handler_filter_ip_subscription_exists' => array(
				'parent' => 'views_handler_filter',
			),
		),
	);
}

/**
 * Implementation of hook_views_data().
 */
function ip_subscription_views_data() {
	
	$data['ip_subscription']['table'] = array(
		'group' => 'IP subscription',
		'join' => array(
			'users' => array(
				'left_field' => 'uid',
				'field' => 'uid'	
			),
			'ip_subscription_address' => array(
				'left_field' => 'sid',
				'field' => 'sid',
			),
		),
	);
	
	$data['ip_subscription_address']['table'] = array(
		'group' => 'IP subscription',
		'join' => array(
			'users' => array(
				'left_table' => 'ip_subscription',
				'left_field' => 'sid',
				'field' => 'sid'	
			),
			'ip_subscription' => array(
				'left_field' => 'sid',
				'field' => 'sid',
			),
		),
	);
	
	$data['ip_subscription']['type'] = array(
		'title' => t('Type'),
		'help' => t('The role granted by the IP subscription.'),
		'field' => array(
			'handler' => 'views_handler_field',
		),
	);
	
	$data['ip_subscription']['sid'] = array(
		'title' => t('IP address'),
		'help' => t('The IP address(es) for this subscription.'),
		'field' => array(
			'handler' => 'views_handler_field_ip_subscription_address',
		),
	);
	
	$data['ip_subscription']['expires'] = array(
		'title' => t('Expires'),
		'help' => t('The date the subscription will expire.'),
		'field' => array(
			'handler' => 'views_handler_field_ip_subscription_expires',
			'click sortable' => TRUE,
		),
		'sort' => array(
			'handler' => 'views_handler_sort_date',
		),
	);
	
	$data['ip_subscription']['exists'] = array(
		'title' => t('Exists'),
		'help' => t('User has an IP subscription.'),
		'filter' => array(
			'handler' => 'views_handler_filter_ip_subscription_exists',
		),
	);
	
	return $data;
}