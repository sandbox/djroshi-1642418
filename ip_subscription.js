Drupal.behaviors.ip_subscription = function(context) {
	$('#edit-sub-select:not(.ip-sub-processed)', context).addClass('ip-sub-processed').bind('change', function(){
		var path = window.location.href;
		$.ajax({
			type: "POST",
			url: path + "/edit",
			data: "type=" + this.options[this.value].text,
			success: function(response) {
				var result = Drupal.parseJson(response);
				console.log(result);
				$('#edit-type-field').val(result.type);
				$('#edit-type').val(result.type);
				$('#edit-ip-address').val(result.ip_address);
				$('#edit-expires').val(result.expires);
			}
		});
		return false;
	});
	$('#view-summary-select:not(.ip-sub-processed)', context).addClass('ip-sub-processed').bind('change', function(){
		var path = window.location.href;
		$("#ip-subscription-content-loader img").removeClass('disable');
		$.ajax({
			type: "POST",
			url: path + "/view",
			data: "type=" + this.value,
			dataType: "html",
			success: function(response) {
				$("#ip-subscription-content-loader img").addClass('disable');
				$('#ip-subscription-summary-container').html(response);	
			}
		});
		return false;
	});
}